#!/usr/bin/env node

process.chdir(__dirname)
process.chdir('..')

var fs = require('fs')
var uglifyJs = require('uglify-js')

var files = [
    'AltitudePanel',
    'AltitudeStatPanel',
    'AltitudeTab',
    'AveragePosition',
    'AverageSpeedPanel',
    'AverageSpeedTab',
    'ClockPanel',
    'ClockTab',
    'CompassPanel',
    'DistanceBetweenPositions',
    'Div',
    'FormatAltitude',
    'HeadingPanel',
    'HeadingTab',
    'ImperialUnit',
    'MainPanel',
    'MaxSpeedPanel',
    'MaxSpeedTab',
    'MetricUnit',
    'OnClick',
    'OneLineTab',
    'Page1Tab',
    'Page2Tab',
    'ResetButton',
    'Settings',
    'SettingsPanel',
    'SettingsTab',
    'SpeedLabel',
    'StartStopButton',
    'StatField',
    'StatusPanel',
    'Tabs',
    'TextNode',
    'TripDistance',
    'TripDistancePanel',
    'TripDistanceTab',
    'TripTimePanel',
    'TripTimeTab',
    'TwoDigitPad',
    'TwoLineTab',
    'WakeLock',
    'Main',
]

var source = '(function () {\n'
files.forEach(function (file) {
    source += fs.readFileSync('js/' + file + '.js', 'utf8') + ';\n'
})
source += '\n})()'

var compressedSource = uglifyJs.minify({ 'combined.js': source }).code

fs.writeFileSync('combined.js', source)
fs.writeFileSync('compressed.js', compressedSource)
